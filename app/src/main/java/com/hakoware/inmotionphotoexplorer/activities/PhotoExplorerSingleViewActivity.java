package com.hakoware.inmotionphotoexplorer.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.hakoware.inmotionphotoexplorer.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PhotoExplorerSingleViewActivity extends AppCompatActivity {

    ImageView selectedImage;
    Bitmap bitmap;
    ProgressDialog pDialog;

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_explorer_single_view);
        selectedImage = (ImageView) findViewById(R.id.selectedImage);
        Intent intent = getIntent();
        String imageURI = intent.getStringExtra("imgURL");
        new LoadImage().execute(imageURI);
        verifyStoragePermissions(this);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Downloading image...", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                saveImage();

            }
        });
    }

    /**
     * Checks if the app has permission to write to device storage. Needed for Marshmallow
     *
     * @param activity
     */
    public static void verifyStoragePermissions(AppCompatActivity activity) {
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    /**
     * Saves the image in a specific directory after a user click of the save button.
     */
    private void saveImage() {
        String APP_PATH_SD_CARD = "/Pictures/InMotion/";
        String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + APP_PATH_SD_CARD;

            try {
                File dir = new File(fullPath);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                OutputStream fOut = null;
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                File file = new File(fullPath, "InMotion_" + timeStamp + ".png");
                fOut = new FileOutputStream(file);

                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                fOut.flush();
                fOut.close();

                MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

            }
            catch (Exception e) {
                Log.e("saveImage()", e.getMessage());
                Toast.makeText(PhotoExplorerSingleViewActivity.this, "There was an error while trying to save the image.", Toast.LENGTH_SHORT).show();
            }

    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PhotoExplorerSingleViewActivity.this);
            pDialog.setMessage("Loading the image ....");
            pDialog.show();

        }
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());

            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {

            if(image != null) {
                selectedImage.setImageBitmap(image);
                pDialog.dismiss();

            }
            else {
                pDialog.dismiss();
                Toast.makeText(PhotoExplorerSingleViewActivity.this, "There was a network error while trying to load the image.", Toast.LENGTH_SHORT).show();

            }
        }
    }

}
