package com.hakoware.inmotionphotoexplorer.activities;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.hakoware.inmotionphotoexplorer.utils.GridItem;
import com.hakoware.inmotionphotoexplorer.utils.GridItemComparator;
import com.hakoware.inmotionphotoexplorer.utils.GridViewAdapter;
import com.hakoware.inmotionphotoexplorer.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PhotoExplorerMainGridViewActivity extends AppCompatActivity {

    // private variables
    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapter mGridAdapter;
    private ArrayList<GridItem> mGridData;

    // url for json data
    private String IMG_URL = "http://jsonplaceholder.typicode.com/photos";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_explorer_main_grid_view);
//        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
//        setSupportActionBar(myToolbar);

        mGridView = (GridView) findViewById(R.id.gridView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        //Initialize with empty data
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapter(this, R.layout.photo_item, mGridData);
        mGridView.setAdapter(mGridAdapter);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                // Start new activity for main view of image, where the user can choose do download or other options.
                Intent intent = new Intent(PhotoExplorerMainGridViewActivity.this, PhotoExplorerSingleViewActivity.class);
                GridItem selectedOne = (GridItem) mGridView.getItemAtPosition(position);
                intent.putExtra("imgTitle", selectedOne.getTitle());
                intent.putExtra("imgURL", selectedOne.getImage());
                startActivity(intent);
            }

        });

        // This makes the url call to get the JSON for the gridView
        new CallURLForJSON().execute(IMG_URL);

    }


    /**
     *
     */
    private class CallURLForJSON extends AsyncTask<String, String, String> {

        /**
         * Method to make the URL connection,get the resulting JSON, and disconnect the URL connection
         *
         * @param params
         * @return
         */
        @Override
        protected String doInBackground(String... params) {
            String urlString = params[0];
            try {
                URL url = new URL(urlString);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    // parseResult to populate the gridView with the JSON data
                    parseResult(stringBuilder.toString());
                    return stringBuilder.toString();
                }
                finally {
                    urlConnection.disconnect();
                }
            }
            catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPreExecute() {
            super.onPreExecute();
            // show processing bar since the data retrieval might take some time
            mProgressBar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(String response) {
            // Only try to update the UI if the data was retrieved
            if (response != null) {
                mGridAdapter.setGridData(mGridData);
            }
            else {
                Toast.makeText(PhotoExplorerMainGridViewActivity.this, "There was an error when retrieving the data.", Toast.LENGTH_SHORT).show();
            }
            mProgressBar.setVisibility(View.GONE);
        }

        /**
         * This method parses the results, gets the title for each image, and the image itself.
         * The method assumes the JSON is formatted with a "title" string and "url" string in a JSON array
         * @param result
         */
        private void parseResult(String result) {
            try {
                JSONArray response = new JSONArray(result);
                GridItem item;
                for (int i = 0; i < response.length(); i++) {
                    JSONObject post = response.optJSONObject(i);
                    String title = post.optString("title");
                    item = new GridItem();
                    item.setTitle(title);
                    String imgURL = post.getString("url");
                    // This is expecting the images to have a url of the example form: http://placehold.it/600/92c952, and this gets the "92c952" to make a direct request later.
                    String imgRecord = imgURL.substring(imgURL.lastIndexOf("/") + 1);
                    // TODO: 2/12/16 - read more about Picasso and image loading from placehold.it. This works for direct to png urls but not placehold.it redirects.
                    item.setImage("https://placeholdit.imgix.net/~text?txtsize=56&bg="+ imgRecord +"&txt=600%C3%97600&w=600&h=600");
                    mGridData.add(item);
                }

                // Sort each grid item based on title, alphabetically, so that we can have meaningful fast scroll functionality. Sample data has about 5,000 records.
                Collections.sort(mGridData, new GridItemComparator());
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
