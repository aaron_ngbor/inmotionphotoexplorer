package com.hakoware.inmotionphotoexplorer.utils;

import java.util.Comparator;

/**
 * Created by aaron on 2/13/16.
 */
public class GridItemComparator implements Comparator<GridItem> {
        @Override
        public int compare(GridItem a, GridItem b) {
            return a.getTitle().compareToIgnoreCase(b.getTitle());
        }
    }

